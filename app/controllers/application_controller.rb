# frozen_string_literal: true

# base api controller
class ApplicationController < ActionController::API
  include Response
end
