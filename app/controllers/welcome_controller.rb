# frozen_string_literal: true

# Default endpoint for API requests
class WelcomeController < ApplicationController
  def index
    welcome = { name: 'Player Character', birthdate: '07/25/2056' }
    json_response(welcome)
  end
end
