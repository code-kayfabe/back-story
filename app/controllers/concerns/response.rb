# frozen_string_literal: true

# response handling functions
module Response
  def json_response(object, status = :ok)
    render json: object, status: status
  end
end
