# frozen_string_literal: true

# base job class
class ApplicationJob < ActiveJob::Base
end
